package org.excel.extra;

import java.util.HashMap;
import java.util.List;

import org.excel.extra.service.UberService;
import org.excel.extra.util.ExcelUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class HomeController {
	
	private static final Logger logger = LoggerFactory.getLogger(HomeController.class);

	@Autowired
	private UberService uberService;
	
	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String home(Model model) {
		
		List<HashMap> map = uberService.getEventList();
		
		ExcelUtil.createExcel(map, "test01");
	
		model.addAttribute("list",  uberService.getEventList());
		return "home";
	}
	
}
