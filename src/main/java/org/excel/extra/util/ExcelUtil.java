package org.excel.extra.util;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.List;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;

public class ExcelUtil {

	public static void createExcel(List<HashMap> list, String fileName) {

		if (!list.isEmpty()) {
			int columnSize = list.get(0).keySet().size();

			Workbook wb = new HSSFWorkbook();
			Sheet sheet1 = wb.createSheet("new sheet");

			Row row = sheet1.createRow((short) 0);
			for (int idx = 0; idx < columnSize; idx++) {
				Cell cell = row.createCell(idx);
				cell.setCellValue(list.get(0).keySet().toArray()[idx].toString());
			}
			for (int idx = 0, size = list.size(); idx < size; idx++) {
				Row rows = sheet1.createRow((short) (idx+1));
				for (int cmn = 0; cmn < columnSize; cmn++) {
					Cell cell = rows.createCell(cmn);
					try {
						cell.setCellValue( new String((""+list.get(idx).values().toArray()[cmn]).getBytes("iso-8859-1"), "euc-kr"));
					} catch (UnsupportedEncodingException e) {
						e.printStackTrace();
					}
					
				}
			}

			FileOutputStream fileOut;
			try {
				fileOut = new FileOutputStream("C:/Users/Snow/Documents/Work/"+fileName+".xls");
				wb.write(fileOut);
				fileOut.close();
				System.out.println("생성되었습니다.");
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

	}

}
