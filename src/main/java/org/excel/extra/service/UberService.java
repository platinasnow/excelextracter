package org.excel.extra.service;

import java.util.HashMap;
import java.util.List;

import org.excel.extra.dao.UberDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UberService {

	@Autowired
	private UberDAO uberDAO;
	
	public List<HashMap> getEventList(){
		return uberDAO.getEventList();
	}
}
